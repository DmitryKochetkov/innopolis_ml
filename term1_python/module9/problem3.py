import base64

s = int(input())
result = base64.urlsafe_b64encode(bytearray(s, encoding='utf-8'))
print(result.decode('ascii'))