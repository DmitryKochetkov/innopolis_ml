from datetime import datetime, timedelta

def days_lived(year, month, day):
    return (datetime.now() - datetime(year, month, day)).days