def fibonacci(index):
    a = 0
    b = 1

    if index == 1:
        return a
    
    if index == 2:
        return b

    i = 2
    while i < index:
        c = a + b
        a = b
        b = c
        i += 1
    
    return b
    
assert fibonacci(1) == 0
assert fibonacci(2) == 1
assert fibonacci(3) == 1
assert fibonacci(4) == 2
assert fibonacci(5) == 3
assert fibonacci(6) == 5
assert fibonacci(7) == 8
assert fibonacci(8) == 13