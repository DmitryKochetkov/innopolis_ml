def last_discharge(a):
    parts = a.split('.')
    if len(parts) == 1:
        return int(a) - 1

    x = 0.1 ** len(parts[1])
    return float(a) - x

print(last_discharge('42'))
print(last_discharge('45.12'))
print(last_discharge('1.00'))
print(last_discharge('1.0'))
