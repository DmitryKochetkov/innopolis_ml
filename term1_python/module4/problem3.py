from collections import defaultdict

path = input()
d = defaultdict(int)

with open(path) as f:
    for line in f.readlines():
        for word in line.split():
            d[word] += 1

for item in sorted(d.items(), key = lambda x: (-x[1], x[0])):
    print(item[1], item[0])