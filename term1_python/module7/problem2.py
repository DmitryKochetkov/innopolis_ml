class Lucas:
    def __init__(self, u0, u1, p, q, n):
        self.u0 = u0
        self.u1 = u1
        self.p = p
        self.q = q
        self.n = n
        self.current = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.current == 0:
            self.current += 1
            return self.u0
        elif self.current == 1:
            self.current += 1
            return self.u1
        elif self.current >= self.n:
            raise StopIteration
        else:
            result = self.p * self.u1 - self.q * self.u0
            self.u0 = self.u1
            self.u1 = result
            self.current += 1
            return result

# print(list(Lucas(0, 1, 1, -1, 10)))
# print(list(Lucas(0, 1, 1, 1, 10)))
# print(list(Lucas(2, 1, 1, -1, 10)))