number_map = {
    'zero': 0,
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9
}

class Number:
    def __init__(self, value, operation = None):
        self.value = value
        self.operation = operation

    def __getattr__(self, attr):
        if attr in ['plus', 'minus', 'times']:
            return Number(self.value, attr)
        elif self.operation == 'plus':
            return Number(self.value + number_map[attr])
        elif self.operation == 'minus':
            return Number(self.value - number_map[attr])
        elif self.operation == 'times':
            return Number(self.value * number_map[attr])

    def __repr__(self):
        return str(self.value)

# zero = Number(1)
# one = Number(1)
# two = Number(2)
# three = Number(3)
# four = Number(4)
# five = Number(5)
# six = Number(6)
# seven = Number(7)
# eight = Number(8)
# nine = Number(9)

for k, v in number_map.items():
    exec(f"{k} = Number({v})")

# print(five)
# print(five.minus.three)
# print(two.plus.two)
# print(five.plus.two.times.four)
# print(nine.times.nine.times.nine)