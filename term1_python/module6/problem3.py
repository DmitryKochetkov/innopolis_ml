class Player:
    id: int
    name: str
    scores: int
    games: list
    
    def __init__(self, id: int, name: str) -> None:
        self.id: int = id
        self.name: str = name
        self.scores: int = 0
        self.games: list = []

    def add_result(self, game_id: int, scores: int) -> None:
        self.games.append(game_id)
        self.scores += scores
    
    def get_mean(self) -> float:
        return self.scores / len(self.games)


class Power:
    name: str
    cost: int
    
    def __init__(self, name, cost):
        self.name = name
        self.cost = cost

    def use(self, player):
        print(f"{player.name} used {self.name}")


class PhysicalPower(Power):
    name: str
    cost: int
    count: int
    
    def __init__(self, name: str, cost: int, count: int):
        super().__init__(name, cost)
        self.count = count

    def use(self, player: Player) -> None:
        if self.count > 0:
            super().use(player)
        else:
            print(f"{player.name} cannot use {self.name}")
        self.count -= 1


class MagicPower(Power):
    name: str
    cost: int
    
    def __init__(self, name: str, cost: int):
        super().__init__(name, cost)

    def use(self, player: Player) -> None:
        super().use(player)
        player.scores += 1

p = Player(1, 'Bilbo')
t = PhysicalPower('teleport', 10, count=1)
t.use(p)
t.use(p)
print(p.scores)

b = MagicPower('black magic', 200)
b.use(p)
print(p.scores)
b.use(p)
print(p.scores)