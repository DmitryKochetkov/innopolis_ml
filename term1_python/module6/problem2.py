class Creature:
    health_points = 100
    def __init__(self) -> None:
        self.health_points = 100

class EarthCreature(Creature):
    def __init__(self) -> None:
        super().__init__()

class SeaCreature(Creature):
    def __init__(self) -> None:
        super().__init__()

class AirCreature(Creature):
    def __init__(self) -> None:
        super().__init__()

class Troll(EarthCreature):
    health_points = 100
    def __init__(self) -> None:
        super().__init__()
        self.health_points = 100

class Elf(EarthCreature):
    health_points = 80
    def __init__(self) -> None:
        super().__init__()
        self.health_points = 80

class Mermaid(SeaCreature):
    health_points = 60
    def __init__(self) -> None:
        super().__init__()
        self.health_points = 60

class Siren(SeaCreature):
    health_points = 75
    def __init__(self) -> None:
        super().__init__()
        self.health_points = 75

class Dragon(AirCreature):
    health_points = 120
    def __init__(self) -> None:
        super().__init__()
        self.health_points = 120

class Pegasus(AirCreature):
    health_points = 85
    def __init__(self) -> None:
        super().__init__()
        self.health_points = 85