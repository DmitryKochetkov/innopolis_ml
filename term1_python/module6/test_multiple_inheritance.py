class B:
    def save(self):
        print('B: save')

class C:
    def save(self):
        print('C: save')

class A(B, C):
    pass

A().save()