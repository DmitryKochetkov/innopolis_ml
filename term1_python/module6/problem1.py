class Player:
    id: int
    name: str
    scores: int
    games: list
    
    def __init__(self, id: int, name: str) -> None:
        self.id: int = id
        self.name: str = name
        self.scores: int = 0
        self.games: list = []

    def add_result(self, game_id: int, scores: int) -> None:
        self.games.append(game_id)
        self.scores += scores
    
    def get_mean(self) -> float:
        return self.scores / len(self.games)
