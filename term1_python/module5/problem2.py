from math import sqrt

class Point3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def distance_to(self, other) -> float:
        return sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2 + (self.z - other.z) ** 2)

class Segment3D:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2

    def length(self):
        return self.p1.distance_to(self.p2)

    def middle(self):
        return Point3D((self.p1.x + self.p2.x) / 2, (self.p1.y + self.p2.y) / 2, (self.p1.z + self.p2.z) / 2)

p1 = Point3D(1, 2, 3)
p2 = Point3D(2.5, 1, -2)
s = Segment3D(p1, p2)
m = s.middle()
assert s.length() == 5.315072906367325
assert m.x == 1.75
assert m.y == 1.5
assert m.z == 0.5

