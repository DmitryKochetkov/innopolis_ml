from math import sqrt

class Point3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def distance_to(self, other) -> float:
        return sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2 + (self.z - other.z) ** 2)

class Segment3D:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
        self.x = p2.x - p1.x
        self.y = p2.y - p1.y
        self.z = p2.z - p1.z

    def length(self):
        return self.p1.distance_to(self.p2)

    def middle(self):
        return Point3D((self.p1.x + self.p2.x) / 2, (self.p1.y + self.p2.y) / 2, (self.p1.z + self.p2.z) / 2)

    def cos_to(self, other):
        product = self.x * other.x + self.y * other.y + self.z * other.z
        return abs(product / (sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2) * sqrt(other.x ** 2 + other.y ** 2 + other.z ** 2)))

