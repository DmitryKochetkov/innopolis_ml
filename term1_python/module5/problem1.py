from math import sqrt

class Point3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def distance_to(self, other) -> float:
        return sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2 + (self.z - other.z) ** 2)

p1 = Point3D(1, 2, 3)
p2 = Point3D(2.5, 1, -2)
assert p1.distance_to(p2) == 5.315072906367325

