def double_print(s: str):
    if s == '':
        raise ValueError("empty string is not allowed")
    print(s)
    print(s)

# double_print("Hello")
# double_print("")