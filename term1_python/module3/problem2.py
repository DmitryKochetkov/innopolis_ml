n = int(input())
points = list()
for i in range(n):
    x, m = map(int, input().split())
    points.append((x, m))

for point in sorted(points, key = lambda point: -point[1]):
    print(point[0])
