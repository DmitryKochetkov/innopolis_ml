domains = {
    'uk': 'Великобритания',
    'de': 'Германия',
    'il': 'Израиль',
    'in': 'Индия',
    'kz': 'Казахстан',
    'mm': 'Мьянма',
    'om': 'Оман',
    'ru': 'Россия',
    'uz': 'Узбекистан',
    'et': 'Эфиопия',
    'com': 'Международный',
    'org': 'Международный',
    'net': 'Международный',
}

def parse_domain(url):
    return domains[url.split('.')[-1]]

url = input()
print(parse_domain(url))