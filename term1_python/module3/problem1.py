def find_min_integer(n):
    digits = ''.join(sorted(str(n)))
    n_zeros = digits.count('0')
    digits = digits.replace('0', '')
    
    return int(digits[0] + '0' * n_zeros + digits[1:])

assert find_min_integer(51) == 15
assert find_min_integer(121) == 112
assert find_min_integer(110) == 101
assert find_min_integer(1100) == 1001
assert find_min_integer(809) == 809

n = int(input())
print(find_min_integer(n))