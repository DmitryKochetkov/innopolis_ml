import numpy as np

def get_dominant_eigenvalue_and_eigenvector(data, num_steps):
    """
    data: np.ndarray – symmetric diagonalizable real-valued matrix
    num_steps: int – number of power method steps
    
    Returns:
    eigenvalue: float – dominant eigenvalue estimation after `num_steps` steps
    eigenvector: np.ndarray – corresponding eigenvector estimation
    """
    ### YOUR CODE HERE

    eigenvalue = 0
    r0 = np.random.rand(data.shape[1])
    for _ in range(num_steps):
        r1 = np.dot(data, r0)
        r1_norm = np.linalg.norm(r1)
        r0 = r1 / r1_norm
        eigenvalue = (r1.T @ data @ r1) / (r1.T @ r1)
    
    return float(eigenvalue), np.array(r0, dtype=float)