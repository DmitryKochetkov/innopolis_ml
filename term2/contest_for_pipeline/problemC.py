def solve(s1, s2):
    if len(s2) > len(s1):
        s2 = s2[:len(s1)]
    d = dict()
    for i in range(len(s1)):
        if i < len(s2):
            d[s1[i]] = s2[i]
        else:
            d[s1[i]] = None
    return sorted(d.items())

if __name__ == '__main__':
    s1 = input()
    s2 = input()
    print(solve(s1, s2))