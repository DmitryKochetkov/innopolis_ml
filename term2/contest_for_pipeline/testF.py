from problemF import pascal_triangle

sample = [
    1,
    1,
    1,
    1,
    2,
    1,
    1,
    3,
    3,
    1,
    1,
    4,
    6,
    4,
    1,
    1,
    5,
    10,
    10,
    5,
    1,
    1,
    6,
    15,
    20,
    15,
    6,
    1,
    1,
    7,
    21,
    35,
    35,
    21,
    7,
    1,
    1,
    8,
    28,
    56,
    70,
    56,
    28,
    8,
    1,
    1,
    9,
    36,
    84,
    126,
    126,
    84,
    36,
    9,
    1,
]

actual = list()
gen = pascal_triangle()
for i in range(len(sample)):
    line = next(gen)
    actual.append(line)

assert sample == actual

print(' '.join([str(item) for item in actual]))