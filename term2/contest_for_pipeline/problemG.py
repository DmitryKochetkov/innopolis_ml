def cache(n):
    class LRU:
        def __init__(self, size) -> None:
            self.data = {}
            self.order = []
            self.size = size

        def add(self, h, result):
            if self.size > 0:
                if len(self.data) < self.size:
                    self.data[h] = result
                    self.order.append(h)
                else:
                    to_be_deleted = self.order.pop(0)
                    del self.data[to_be_deleted]
                    self.data[h] = result
                    self.order.append(h)

    def cache_decorated(func):
        lru = LRU(n)
        def wrapper(*args, **kwargs):
            key = args
            if kwargs:
                for item in kwargs.items():
                    key += item
            key += tuple(type(v) for v in args)
            if kwargs:
                key += tuple(type(v) for v in kwargs.values())
            h = hash(key)
            # print('LRU:', lru.data)
            if h in lru.data:
                # print(f'found hashed value {lru.data[h]} with hash {h}')
                return lru.data[h]
            
            result = func(*args, **kwargs)
            lru.add(hash(key), result)
            return result
        return wrapper
    return cache_decorated