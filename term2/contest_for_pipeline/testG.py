from problemG import cache

@cache(2)
def foo(value):
    print('calculate foo for {}'.format(value))
    return value

@cache(5)
def foo2(k, b, *args, **kwargs):
    print(args)
    print(kwargs)
    return k * 2 + b

@cache(0)
def foo3(x):
    return x

foo(1)
foo(2)
foo(1)
foo(2)
foo(3)
x = foo(1)
print(x)


print(foo2(2, 1))
print(foo2(1, 10, 2, r = 3, m = 8))

foo3(1)