from problemC import solve

assert solve('1234', 'qwer') == [('1', 'q'), ('2', 'w'), ('3', 'e'), ('4', 'r')]
assert solve('123', 'qwer') == [('1', 'q'), ('2', 'w'), ('3', 'e')]
assert solve('1234', 'qwe') == [('1', 'q'), ('2', 'w'), ('3', 'e'), ('4', None)]
assert solve('', '') == []
