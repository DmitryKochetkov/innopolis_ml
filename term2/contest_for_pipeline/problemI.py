from app import VeryImportantClass, decorator
from collections import abc

class HackedClass(VeryImportantClass):
    def __init__(self, *args) -> None:
        super().__init__(*args)

    def __getattribute__(self, __name: str):
        attr = super().__getattribute__(__name)
        if not __name.startswith('_'):
            if callable(attr):
                return decorator(attr)
            elif isinstance(attr, int | float):
                return attr * 2
            elif isinstance(attr, abc.Iterable):
                t = type(attr)
                return t()
            else:
                return attr
        else:
            return attr

    