def pascal_triangle():
    i = 0
    line = 0
    prev = [1]
    while True:
        if i < len(prev):
            i += 1
            yield prev[i-1]
        else:
            line += 1
            new_line = [0] * (line + 1)
            new_line[0] = 1
            new_line[-1] = 1
            for i in range(1, line):
                new_line[i] = prev[i-1] + prev[i]

            yield prev[0]
            prev = new_line.copy()
            i = 1
