class VeryImportantClass:
    def __init__(self, a, b) -> None:
        self.a = a
        self.b = b
        self.s = "String attribute"
        self.l = [1, 2, 3]
        self.t = [1, 2]

    def add(self):
        return self.a + self.b
    
    def change(self):
        self.l += [4, 5, 6]
        self.t.append(6)
    

def decorator(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        print(f'Hacked {func}')
    return wrapper