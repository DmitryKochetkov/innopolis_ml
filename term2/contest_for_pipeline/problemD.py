def check_password(s: str):
    return any([c.islower() for c in s]) and any([c.isupper() for c in s]) and any([c.isdecimal() for c in s]) and len(s) >= 8 and 'anna' not in s.lower() and len(set(s)) >= 4

if __name__ == '__main__':
    s = input()
    if check_password(s):
        print('strong')
    else:
        print('weak')
