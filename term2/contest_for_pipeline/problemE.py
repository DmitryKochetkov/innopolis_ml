def check_prime(x):
    n = 2
    while n * n <= x:
        if x % n == 0:
            return False
        n += 1

    return True

def ith_prime(i):
    if i == 1:
        return 2
    
    c = 1
    n = 1
    while n < i:
        c += 2
        if check_prime(c):
            n += 1
        if n == i:
            return c

if __name__ == "__main__":
    i = int(input())
    print(ith_prime(i))