from problemH import ExtendedList

a = ExtendedList([1, 2, 3, 100, 5, -1])
print('Initial')
print(a)
print(a.reversed)
print(a.first)
print(a.last)
print(a.size)

print('After changes')
a.remove(100)
a.remove(1)
print(a)
print(a.reversed)
print(a.first)
print(a.last)
print(a.size)

print('Editing size')
a.size += 3
print(a)
a.size -= 4
print(a)
