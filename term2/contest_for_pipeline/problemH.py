import sys

class ExtendedList(list):
    def __init__(self, *args):
        super().__init__(*args)

    @property
    def reversed(self):
        return self[::-1]
    
    @property
    def first(self):
        return self[0]
    
    @property
    def last(self):
        return self[-1]
    
    @property
    def size(self):
        return len(self)
    
    @size.setter
    def size(self, value):
        delta = value - self.size
        if delta > 0:
            for _ in range(delta):
                self.append(None)
        else:
            for _ in range(abs(delta)):
                self.pop()

exec(sys.stdin.read())