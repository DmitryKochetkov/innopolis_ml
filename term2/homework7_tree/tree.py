import numpy as np
from sklearn.base import BaseEstimator


def entropy(y):  
    """
    Computes entropy of the provided distribution. Use log(value + eps) for numerical stability
    
    Parameters
    ----------
    y : np.array of type float with shape (n_objects, n_classes)
        One-hot representation of class labels for corresponding subset
    
    Returns
    -------
    float
        Entropy of the provided subset
    """
    EPS = 0.0005
    probs = np.sum(y, axis=0) / y.shape[0]
    
    return -np.sum(probs * np.log(probs + EPS))
    
def gini(y):
    """
    Computes the Gini impurity of the provided distribution
    
    Parameters
    ----------
    y : np.array of type float with shape (n_objects, n_classes)
        One-hot representation of class labels for corresponding subset
    
    Returns
    -------
    float
        Gini impurity of the provided subset
    """
    probs = np.sum(y, axis=0) / y.shape[0]
    return 1 - np.sum(probs ** 2)
    
def variance(y):
    """
    Computes the variance the provided target values subset
    
    Parameters
    ----------
    y : np.array of type float with shape (n_objects, 1)
        Target values vector
    
    Returns
    -------
    float
        Variance of the provided target vector
    """
    
    return np.mean((y - np.mean(y)) ** 2)
    # return np.var(y)

def mad_median(y):
    """
    Computes the mean absolute deviation from the median in the
    provided target values subset
    
    Parameters
    ----------
    y : np.array of type float with shape (n_objects, 1)
        Target values vector
    
    Returns
    -------
    float
        Mean absolute deviation from the median in the provided vector
    """
    return np.mean(np.abs(y - np.median(y)))


def one_hot_encode(n_classes, y):
    y_one_hot = np.zeros((len(y), n_classes), dtype=float)
    y_one_hot[np.arange(len(y)), y.astype(int)[:, 0]] = 1.
    return y_one_hot


def one_hot_decode(y_one_hot):
    return y_one_hot.argmax(axis=1)[:, None]


class Node:
    """
    This class is provided "as is" and it is not mandatory to it use in your code.
    """
    def __init__(self, feature_index=None, threshold=None, proba=0):
        self.feature_index = feature_index
        self.value = threshold
        self.proba = proba
        self.left_child = None
        self.right_child = None
    
    def __str__(self) -> str:
        return f'Node(feature_index={self.feature_index}, threshold={self.value}, is_leaf = {self.left_child is None})'
    
    def __repr__(self) -> str:
        return self.__str__()
        
class DecisionTree(BaseEstimator):
    all_criterions = {
        'gini': (gini, True), # (criterion, classification flag)
        'entropy': (entropy, True),
        'variance': (variance, False),
        'mad_median': (mad_median, False)
    }

    def __init__(self, n_classes=None, max_depth=np.inf, min_samples_split=2, 
                 criterion_name='gini', debug=False):

        assert criterion_name in self.all_criterions.keys(), 'Criterion name must be on of the following: {}'.format(self.all_criterions.keys())
        
        self.n_classes = n_classes
        self.max_depth = max_depth
        self.min_samples_split = min_samples_split
        self.criterion_name = criterion_name

        self.depth = 0
        self.root = None # Use the Node class to initialize it later
        self.debug = debug

        
        
    def make_split(self, feature_index, threshold, X_subset, y_subset):
        """
        Makes split of the provided data subset and target values using provided feature and threshold
        
        Parameters
        ----------
        feature_index : int
            Index of feature to make split with

        threshold : float
            Threshold value to perform split

        X_subset : np.array of type float with shape (n_objects, n_features)
            Feature matrix representing the selected subset

        y_subset : np.array of type float with shape (n_objects, n_classes) in classification 
                   (n_objects, 1) in regression 
            One-hot representation of class labels for corresponding subset
        
        Returns
        -------
        (X_left, y_left) : tuple of np.arrays of same type as input X_subset and y_subset
            Part of the providev subset where selected feature x^j < threshold
        (X_right, y_right) : tuple of np.arrays of same type as input X_subset and y_subset
            Part of the providev subset where selected feature x^j >= threshold
        """
        if threshold is None:
            raise ValueError('threshold is None')
        if X_subset is None:
            raise ValueError('X is None')

        X_left = X_subset[X_subset[:, feature_index] < threshold]
        X_right = X_subset[X_subset[:, feature_index] >= threshold]
        y_left = y_subset[X_subset[:, feature_index] < threshold]
        y_right = y_subset[X_subset[:, feature_index] >= threshold]
        # ind_left = np.where(X_subset[:, feature_index] < threshold)
        # ind_right = np.where(X_subset[:, feature_index] >= threshold)
        # X_left = X_subset[ind_left]
        # X_right = X_subset[ind_right]
        # y_left = y_subset[ind_left]
        # y_right = y_subset[ind_right]
        
        return (X_left, y_left), (X_right, y_right)
    
    def make_split_only_y(self, feature_index, threshold, X_subset, y_subset):
        """
        Split only target values into two subsets with specified feature and threshold
        
        Parameters
        ----------
        feature_index : int
            Index of feature to make split with

        threshold : float
            Threshold value to perform split

        X_subset : np.array of type float with shape (n_objects, n_features)
            Feature matrix representing the selected subset

        y_subset : np.array of type float with shape (n_objects, n_classes) in classification 
                   (n_objects, 1) in regression 
            One-hot representation of class labels for corresponding subset
        
        Returns
        -------
        y_left : np.array of type float with shape (n_objects_left, n_classes) in classification 
                   (n_objects, 1) in regression 
            Part of the provided subset where selected feature x^j < threshold

        y_right : np.array of type float with shape (n_objects_right, n_classes) in classification 
                   (n_objects, 1) in regression 
            Part of the provided subset where selected feature x^j >= threshold
        """
        if threshold is None:
            raise ValueError('threshold is None')
        if X_subset is None:
            raise ValueError('X is None')
            
        y_left = y_subset[X_subset[:, feature_index] < threshold]
        y_right = y_subset[X_subset[:, feature_index] >= threshold]
        
        return y_left, y_right

    def choose_best_split(self, X_subset, y_subset):
        """
        Greedily select the best feature and best threshold w.r.t. selected criterion
        
        Parameters
        ----------
        X_subset : np.array of type float with shape (n_objects, n_features)
            Feature matrix representing the selected subset

        y_subset : np.array of type float with shape (n_objects, n_classes) in classification 
                   (n_objects, 1) in regression 
            One-hot representation of class labels or target values for corresponding subset
        
        Returns
        -------
        feature_index : int
            Index of feature to make split with

        threshold : float
            Threshold value to perform split

        """
        # YOUR CODE HERE
        criterion, classification_flag = self.all_criterions[self.criterion_name]

        best_feature_index = 0
        best_threshold = 0
        best_left_size = None
        best_right_size = None
        opt_gain = None

        for feature_index in range(X_subset.shape[1]):
            # пробуем выбрать эту фичу для разбиения    
            thresholds = X_subset[:, feature_index]
            thresholds = np.sort(np.unique(thresholds))
            if len(thresholds) >= 50:
                thresholds = [thresholds[int(i * len(thresholds) / 50.)] for i in range(50)]
            
            for threshold in thresholds:
                y_left, y_right = self.make_split_only_y(feature_index, threshold, X_subset, y_subset)
                if y_left.shape[0] == 0 or y_right.shape[0] == 0:
                    continue
                gain = criterion(y_subset) - (y_left.shape[0] / y_subset.shape[0]) * criterion(y_left) - (y_right.shape[0] / y_subset.shape[0]) * criterion(y_right)
                if opt_gain is None or gain > opt_gain:
                    opt_gain = gain
                    best_feature_index = feature_index
                    best_threshold = threshold 
                    best_left_size = y_left.shape[0]
                    best_right_size = y_right.shape[0]

        if best_feature_index is None:
            print(f'CRITICAL ERROR! choose_best_split is about to return None with thresholds: {thresholds}')
            print(f'X: {X_subset}')
            print(f'y: {y_subset}')
            print(thresholds)
        print(f'Best split found: feature {best_feature_index}, threshold {best_threshold}. Left size: {best_left_size}, right size: {best_right_size}')
        return best_feature_index, best_threshold
    
    def make_tree(self, X_subset, y_subset, current_depth = 1):
        """
        Recursively builds the tree
        
        Parameters
        ----------
        X_subset : np.array of type float with shape (n_objects, n_features)
            Feature matrix representing the selected subset

        y_subset : np.array of type float with shape (n_objects, n_classes) in classification 
                   (n_objects, 1) in regression 
            One-hot representation of class labels or target values for corresponding subset
        
        Returns
        -------
        root_node : Node class instance
            Node of the root of the fitted tree
        """

        node = Node()

        if current_depth == self.max_depth or X_subset.shape[0] < self.min_samples_split:
            if self.classification:
                probs = np.zeros(self.n_classes)
                probs = np.sum(y_subset, axis=0)
                node.y_values = np.argmax(probs)
                node.proba = probs / np.sum(probs)
            else:
                node.y_values = np.mean(y_subset)
            return node
        
        feature_index, threshold = self.choose_best_split(X_subset, y_subset)
        print(f'X_subset.shape: {X_subset.shape}. Best split: feature {feature_index}, threshold {threshold}')
        node = Node(feature_index, threshold)
        X, y = self.make_split(feature_index, threshold, X_subset, y_subset)
        X_left, y_left = X
        X_right, y_right = y

        if X_left.shape[0] == 0 or X_left.shape[0] == 0 or self.criterion(y_subset) == 0:
            if self.classification:
                probs = np.zeros(self.n_classes)
                probs = np.sum(y_subset, axis=0)
                node.y_values = np.argmax(probs)
                node.proba = probs / np.sum(probs)
            else:
                node.y_values = np.mean(y_subset)
            return node
        
        # if y_left.shape[0] > 0:
        node.left_child = self.make_tree(X_left, y_left, current_depth+1)
        # if y_right.shape[0] > 0:
        node.right_child = self.make_tree(X_right, y_right, current_depth+1)
        
        return node
    
    # def make_tree(self, X_subset, y_subset, depth = 1):
    #     """
    #     Recursively builds the tree

    #     Parameters
    #     ----------
    #     X_subset : np.array of type float with shape (n_objects, n_features)
    #         Feature matrix representing the selected subset
    #     y_subset : np.array of type float with shape (n_objects, n_classes) in classification
    #                (n_objects, 1) in regression
    #         One-hot representation of class labels or target values for corresponding subset

    #     Returns
    #     -------
    #     root_node : Node class instance
    #         Node of the root of the fitted tree
    #     """

    #     new_node = Node()

    #     if depth == self.max_depth or len(y_subset) < self.min_samples_split:
    #         if self.classification:
    #             probs = np.zeros(self.n_classes)
    #             probs = np.sum(y_subset, axis=0)
    #             new_node.y_values = np.argmax(probs)
    #             new_node.proba = probs / np.sum(probs)
    #         else:
    #             new_node.y_values = np.mean(y_subset)

    #         return new_node

    #     feature_index, threshold = self.choose_best_split(X_subset, y_subset)
    #     new_node.feature_index = feature_index
    #     new_node.threshold = threshold

    #     left, right = self.make_split(feature_index, threshold, X_subset, y_subset)

    #     if left[1].shape[0] == 0 or right[1].shape[0] == 0 or self.criterion(y_subset) == 0:
    #         if self.classification:
    #             probs = np.zeros(self.n_classes)
    #             probs = np.sum(y_subset, axis=0)
    #             new_node.y_values = np.argmax(probs)
    #             new_node.proba = probs / np.sum(probs)
    #         else:
    #             new_node.y_values = np.mean(y_subset)
    #         return new_node

    #     new_node.left_child = self.make_tree(left[0], left[1], depth + 1)
    #     new_node.right_child = self.make_tree(right[0], right[1], depth + 1)

    #     return new_node
        
    def fit(self, X, y):
        """
        Fit the model from scratch using the provided data
        
        Parameters
        ----------
        X : np.array of type float with shape (n_objects, n_features)
            Feature matrix representing the data to train on

        y : np.array of type int with shape (n_objects, 1) in classification 
                   of type float with shape (n_objects, 1) in regression 
            Column vector of class labels in classification or target values in regression
        
        """
        assert len(y.shape) == 2 and len(y) == len(X), 'Wrong y shape'
        self.criterion, self.classification = self.all_criterions[self.criterion_name]
        if self.classification:
            if self.n_classes is None:
                self.n_classes = len(np.unique(y))
            y = one_hot_encode(self.n_classes, y)

        self.root = self.make_tree(X, y)
    
    def predict(self, X):
        """
        Predict the target value or class label  the model from scratch using the provided data
        
        Parameters
        ----------
        X : np.array of type float with shape (n_objects, n_features)
            Feature matrix representing the data the predictions should be provided for

        Returns
        -------
        y_predicted : np.array of type int with shape (n_objects, 1) in classification 
                   (n_objects, 1) in regression 
            Column vector of class labels in classification or target values in regression
        
        """
        return [self.predict_instance(x, self.root) for x in X]
    
    def predict_instance(self, x, node):
        if node.left_child == None:
            return node.y_values
        if x[node.feature_index] < node.value:
            # print('Choosing left node')
            return self.predict_instance(x, node.left_child)
        else:
            # print('Choosing right node')
            return self.predict_instance(x, node.right_child)
    
    def predict_proba_instance(self, x, node):
        if node.left_child == None:
            return node.proba
        if x[node.feature_index] < node.value:
            return self.predict_proba_instance(x, node.left_child)
        return self.predict_proba_instance(x, node.right_child)
        
    def predict_proba(self, X):
        """
        Only for classification
        Predict the class probabilities using the provided data
        
        Parameters
        ----------
        X : np.array of type float with shape (n_objects, n_features)
            Feature matrix representing the data the predictions should be provided for

        Returns
        -------
        y_predicted_probs : np.array of type float with shape (n_objects, n_classes)
            Probabilities of each class for the provided objects
        
        """
        assert self.classification, 'Available only for classification problem'

        # YOUR CODE HERE
        
        return np.array([self.predict_proba_instance(x, self.root) for x in X])