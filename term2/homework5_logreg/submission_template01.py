import numpy as np
import torch
from torch import nn

def create_model():
    # Linear layer mapping from 784 features, so it should be 784->256->16->10
    
    return nn.Sequential(
        nn.Linear(784, 256, bias=True),
        nn.ReLU(),
        nn.Linear(256, 16, bias=True),
        nn.ReLU(),
        nn.Linear(16, 10, bias=True)
    )

def count_parameters(model):
    # your code here
    # return integer number (None is just a placeholder)
    result = 0
    for param in model.parameters():
        result += np.prod([*param.size()])
    return result